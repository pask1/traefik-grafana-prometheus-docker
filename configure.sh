#!/bin/bash
# Copyright (C) 2022 Matt Jolly

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Traefik/Grafana/Prometheus stack configuration script.
# This script will use `sed` to substitute replacement variables to configure the URIs
# that you want your services to run on. It will also configure the `admin` user password for Grafana

# The default configuration (and the way that this configuration is envisioned to be used) is to have traefik on a subdomain for your swarm.
# E.g. '{traefik,grafana,prometheus}.docker.domain.tld'. Other configurations are easily accommodated, you just need to adjust the "rule=Host(`..."
# in the docker-compose file manually, as well as grafana's default datasource.

echo "Please enter the subdomain UNDER which you want the traefik stack to run."
echo "E.g. for '{traefik,grafana,prometheus}.docker.domain.tld' enter 'docker.domain.tld'"

read DOMAIN

printf "\n"
echo "Please enter the password for your Grafana 'admin' user"
read -sr GRAFANA_PASSWORD
# Escape any special characters in the password
ESCAPED_GRAFANA_PASSWORD=$(sed -e 's/[$\/&]/\\&/g' <<< $GRAFANA_PASSWORD)

printf "\n"
echo "Please enter your email address (for LetsEncrypt certificate generation)"
read LEGO_EMAIL
ESCAPED_LEGO_EMAIL=$(sed -e 's/[$\/&]/\\&/g' <<< $LEGO_EMAIL)

printf "\n"
echo "Please enter your LEGO DNS provider."
echo "See https://go-acme.github.io/lego/dns/ for a complete list; if left empty this configuration item will be skipped."
read DNS_PROVIDER



printf "\n"
echo "Configuring your Grafana instance."
sed -i "s/YOUR_HOSTNAME_HERE/${DOMAIN}/g" grafana/provisioning/datasources/datasource.yml
sed -i "s/GRAFANA_ADMIN_PASSWORD/${ESCAPED_GRAFANA_PASSWORD}/g" grafana/config.monitoring

echo "Configuring docker-compose.yml for your environment."
sed -i "s/YOUR_HOSTNAME_HERE/${DOMAIN}/g" docker-compose.yml

echo "Configuring traefik.toml for LetsEncrypt"
sed -i "s/YOUR_EMAIL_ADDRESS/${LEGO_EMAIL}/g" traefik.toml

if [ -z "$DNS_PROVIDER" ]
then
  echo "DNS provider was not set. If you wish to use LetsEncrypt please edit traefik.toml manually."
else
  sed -i "s/cloudflare/${DNS_PROVIDER}/g" traefik.toml
fi

unset GRAFANA_ADMIN_PASSWORD
unset GRAFANA_PASSWORD

echo "Done. You should be able to deploy the stack using:"
echo "'docker stack deploy -c docker-compose.yml traefik'"
