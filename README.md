# README

This repository contains a docker stack (named docker-compose.yml for docker autocompletion), along with helper scripts, that will do most of the work to setup a [Traefik](https://traefik.io/)+[Grafana](https://grafana.com/)+[Prometheus](https://prometheus.io/) stack and enable you to manage other docker services.

I've run 20+ services on a (rather large) single node docker swarm using this configuration; it's easy to deploy and maintain.

This stack is designed to be deployed (via the configuration script) under a single subdomain (e.g. `{traefik,grafana,prometheus}.docker.domain.tld`). With minimal manual work you can deploy this stack customised to suit your environment.

Setting up appropriate DNS (or hosts for local testing) entries for these URIs is **required**, as Traefik uses the URI of received requests to route your traffic to its destination, but is beyond the scope of this readme.

> **Note**: This stack is setup to use LetsEncrypt/LEGO DNS challenge for certificate generation as it is the only mechanism that supports wildcard.

## Usage

The included configuration script can be used to set most of the values that must be changed in any given environment, however for more customisation you may manually configure the files listed under [Advanced].

### Simple

The simplest way to use this repository is to check it out and run the `configure.sh` script.

```bash
# Pull down the repo to /data (or you'll need to modify some bind mounts)
git clone https://gitlab.com/Matt.Jolly/traefik-grafana-prometheus-docker.git traefik
# If you aren't already in swarm mode, run this:
docker swarm init
# Create an external network for Traefik
docker network create -d overlay --scope swarm traefik-network
# Configure the domains that Traefik is listening for:
./configure.sh
# Start the stack
docker stack deploy -c docker-compose.yml traefik
```

This will **not** configure HTTPS/SSL certificate generation via LetsEncrypt and will instead use self-signed certificates. Please follow the steps in the [LetsEncrypt] section below to enable this.

### Advanced

1. (If required) initialise your Docker swarm.
2. Create a docker network for Traefik: `docker network create -d overlay --scope swarm traefik-network`
3. Modify `docker-compose.yml`, `grafana/config.monitoring` and `grafana/provisioning/datasources/datasource.yml`, and `traefik.toml` to suit your environment (or use `configure.sh`).
4. Configure [LetsEncrypt].
5. Deploy the stack.
6. ???
7. Profit!


## LetsEncrypt

LetsEncrypt may be used with this stack to request and automatically renew DNS certificates.

If your provider supports `_FILE` environment variable names these should be used (in combination with Docker secrets) rather than dumping environment variables in your Docker stack file.

The following steps must be followed to enable LetsEncrypt (using the example of my provider, `cloudflare`):

1. Identify your [LEGO DNS provider](https://go-acme.github.io/lego/dns/)

2. Note the `code`; and any "Credential" environment variables required to configure this provider; for this example:
   - `code`: cloudflare
   - CF_DNS_API_TOKEN_FILE: /run/secrets/traefik-cf-api-token
   - CF_API_EMAIL: Associated@email.address

3. (If `_FILE` variables supported) Create Docker secrets for your variables, e.g.:
   ```bash
   printf "your-api-key" | docker secret create traefik-cf-api-token -
   ```

4. If this was not done earlier (using the configuration script), modify `traefik.toml` so that it contains your DNS provider.
 
5. Modify `docker-compose.yml` to include your secret names and assign the secrets to the variables (using the existing variables as a reference)

6. (Optional) Set the log level to `DEBUG` in `traefik.toml`, and enable the "Staging CA" by uncommenting it's line. These settings may be reverted once certificates are generating successfully to help you avoid being rate-limited.

7. Redeploy your Docker stack with the new settings.

## Configuring other services to use Traefik

Traefik can be dynamically configured as Docker containers are created and destroyed using Docker `labels`. An example of this is the following (sanitised) [MinIO](https://min.io) stack:

```yaml
version: "3.9"

services:
  minio:
    image: minio/minio:latest
    environment:
      - MINIO_ROOT_USER=username
      - MINIO_ROOT_PASSWORD=password
    command: server /data --console-address ":9001"
    deploy:
      labels:
        # Enable Traefik for this service
        - "traefik.enable=true"
        - "traefik.docker.network=traefik-network"
      
        # Listen on port 80 (HTTP) to redirect to https
        - "traefik.http.routers.minio-ui-web.rule=Host(`minio.lab.footclan.ninja`)"
        - "traefik.http.routers.minio-ui-web.entrypoints=web"
        - "traefik.http.routers.minio-ui-web.middlewares=redirect-https@file"
        - "traefik.http.routers.minio-ui-web.service=minio-ui"
      
        # Listen on port 443 (HTTPS) for connections to the console
        - "traefik.http.routers.minio-ui-websecure.rule=Host(`minio.lab.footclan.ninja`)"
        - "traefik.http.routers.minio-ui-websecure.entrypoints=websecure"
        - "traefik.http.routers.minio-ui-websecure.tls=true"
        # Configure automatic certificate generation for the service
        # In this case there's no reason to waste LetsEncrypt's resources generating two certificates
        # when we generate a wildcard for *.minio and re-use that certificate for the API service
        - "traefik.http.routers.minio-ui-websecure.tls.certresolver=letsencrypt"
        - "traefik.http.routers.minio-ui-websecure.tls.domains[0].main=minio.lab.footclan.ninja"
        - "traefik.http.routers.minio-ui-websecure.tls.domains[0].sans=*.minio.lab.footclan.ninja"
        - "traefik.http.routers.minio-ui-websecure.service=minio-ui"

        # Listen on port 443 (HTTPS) for connections to the API
        - "traefik.http.routers.minio-api-websecure.rule=Host(`api.minio.lab.footclan.ninja`)"
        - "traefik.http.routers.minio-api-websecure.entrypoints=websecure"
        - "traefik.http.routers.minio-api-websecure.tls=true"
        - "traefik.http.routers.minio-api-websecure.tls.certresolver=letsencrypt" 
        - "traefik.http.routers.minio-api-websecure.tls.domains[0].main=minio.lab.footclan.ninja"
        - "traefik.http.routers.minio-api-websecure.tls.domains[0].sans=*.minio.lab.footclan.ninja"
        - "traefik.http.routers.minio-api-websecure.service=minio-api"
        
        # Define where the http services are to be routed (ports the container is listening on)
        - "traefik.http.services.minio-ui.loadbalancer.server.port=9001"
        - "traefik.http.services.minio-api.loadbalancer.server.port=9000"
    networks:
      - traefik-network
    volumes:
      - /data/minio/data:/data

networks:
  traefik-network:
    external: true
```

Once this stack is deployed (on a swarm that has Traefik), requests will be routed to the exposed services automatically based on the `Host` rule.

- http://minio.lab.footclan.ninja will redirect to https://minio.lab.footclan.ninja
- https://minio.lab.footclan.ninja will have its SSL connection terminated at Traefik and its data will be transmitted to port 9001 (`minio-ui` service) on the MinIO container.
- https://api.minio.lab.footclan.ninja will have its SSL connection terminated at Traefik and its data will be transmitted to port 9000 (`minio-api` service) on the MinIO container.

Note that the only exposed ports in this entire configuration are done on the Traefik container - no other containers are directly exposed to the world.

This Traefik instance is configured so that services must explicitly enable its use through the `traefik.enable=true` label.

A reference for Traefik labels can be found [here](https://doc.traefik.io/traefik/providers/docker/).

## Troubleshooting

1. I'm not getting a LetsEncrypt Certificate and I've followed the instructions above:
   
   - Change the log level to `DEBUG` in `traefik.toml` and restart the stack. You will be provided with appropriate logs to diagnose the issue.

2. How do I view the logs?
   
   - `docker service logs -f traefik_traefik`
